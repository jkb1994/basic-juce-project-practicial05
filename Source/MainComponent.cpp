/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //Text Button01
    textButton1.setButtonText("Click Me1!");
    textButton1.addListener(this);
    addAndMakeVisible(&textButton1);
    
    //Text Button02
    textButton2.setButtonText("Click Me2!");
    textButton2.addListener(this);
    addAndMakeVisible(&textButton2);
    
    //Slider
    slider1.setSliderStyle(Slider:: Rotary);
    slider1.addListener(this);
    addAndMakeVisible(&slider1);
    
    //ComboBox
    comboBox1.addItem("Item3", 3);
    addAndMakeVisible(&comboBox1);
    
    //TextEditor
    textEditor1.setText("kajhsfka");
    addAndMakeVisible(&textEditor1);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("Width = " << getWidth() << "\n");
    DBG("Height = " << getHeight() << "\n");
    
    // Pos from left, pos from top, width, height
    
    /*textButton1.setBounds(10, 10, getWidth() -20, 40);
    textButton2.setBounds(10, 50, getWidth() -20, 40);
    
    slider1.setBounds(10, 90, getWidth() - 20, 40);
    
    comboBox1.setBounds(10, 130, getWidth() - 20, 40);
    
    textEditor1.setBounds(10, 170, getWidth() - 20, 40);*/
}


void MainComponent:: buttonClicked(Button* button)
{
    if (button == &textButton1)
        DBG("textButton1 Clicked\n");
    else if (button == &textButton2)
        DBG("textButton2 Clicked\n");
    
}

void MainComponent:: sliderValueChanged(Slider* slider)
{
    DBG("Slider Change\n");
}
void MainComponent::paint(Graphics& g)
{
    g.setColour(Colours::blueviolet);
    //g.fillAll();
    g.fillRect(0, 0, getWidth()/2.0, getHeight()/2.0);
    g.fillRect(250, 200, getWidth()/2.0, getHeight()/2.0);
    //g.drawEllipse(0, 0, getWidth(), getHeight(), 1);
    //g.fillRoundedRectangle(getWidth()/2.0, getHeight()/2.0, getWidth()/2, getHeight()/2, 2);
    //g.drawHorizontalLine(getHeight()/2.0, 0, getWidth());
    //g.drawSingleLineText("SDA!", 100, 100);
    
    
}
